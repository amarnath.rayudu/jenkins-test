package com.target.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.target.pages.base.BasePage;

public class LoginPage extends BasePage {
	
	By emailBox = By.id("logonId");
	By passwordBox = By.id("logonPassword");
	By signInButton = By.xpath("//button[@type='submit']");
	

	public LoginPage(WebDriver driver) throws InterruptedException {
		super(driver);
		waitForElement(emailBox);
		// TODO Auto-generated constructor stub
	}
	
	public boolean isloginPageLoaded(){
		return driver.findElements(emailBox).size()>0;
	}
	public Homepage login(String username,String password) throws Exception{
		driver.findElement(emailBox).sendKeys(username);
		driver.findElement(passwordBox).sendKeys(password);
		driver.findElement(signInButton).click();
		return new Homepage(driver);
	}
	
}
