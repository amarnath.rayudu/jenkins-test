package com.target.pages.base;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.target.pages.Homepage;

public class BasePage {

	By profileLocator = By.xpath("//a[@id='OpenLogedinPopup']");
	By logoutLocator = By.linkText("sign out");
	By signInButton = By.xpath("//button[@type='submit']");
	By targetLink = By.xpath("//div/div/a");
	By noThanksLocator = By.linkText("No, thanks");
	public static WebDriver driver;

	public BasePage(WebDriver driver) {
		this.driver = driver;
	}

	public static void waitForElement(By locator) throws InterruptedException {
		for (int i = 0; i < 600; i++) {
			if (driver.findElements(locator).size() > 0)
				break;
			Thread.sleep(100);

		}
	}

	public boolean isElementPresent(By locator) {

		boolean isPresent = driver.findElements(locator).size() > 0;

		return isPresent;
	}

	public Homepage navigateToHomePage() throws Exception {
		driver.findElement(targetLink).click();
		Thread.sleep(9000);
		return new Homepage(driver);
	}

	public void logout() throws Exception {
		System.out.println(" VIN 333333 ");
		if (isElementPresent(noThanksLocator)) {
			driver.findElement(noThanksLocator).click();
		}
		Thread.sleep(3000);
		driver.findElement(profileLocator).click();
		driver.findElement(logoutLocator).click();
		waitForElement(signInButton);

	}
}
