package com.target.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.target.pages.base.BasePage;

public class AccountPage extends BasePage {
	
	By name = By.id("nameInfo");

	public AccountPage(WebDriver driver) throws InterruptedException {
		super(driver);
		waitForElement(name);
		// TODO Auto-generated constructor stub
	}
	
	public boolean isAccountPageLoaded(){
		return driver.findElements(name).size()>0;				
	}
	

}
