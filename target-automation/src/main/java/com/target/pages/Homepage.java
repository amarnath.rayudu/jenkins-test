package com.target.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.target.pages.base.BasePage;

public class Homepage extends BasePage {
	
	By signInLink = By.id("OpenLoginPopup");
	By user = By.id("OpenLogedinPopup");
	By manageAccountButton = By.id("manageAccount");

	public Homepage(WebDriver driver) throws InterruptedException {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	
	public LoginPage navigateToLogin() throws InterruptedException{
		driver.findElement(signInLink).click();
		return new LoginPage(driver);
	
	}
	
	public AccountPage navigateToAccountPage() throws InterruptedException{
		driver.findElement(user).click();
		driver.findElement(manageAccountButton).click();
		return new AccountPage(driver);
	}
}
