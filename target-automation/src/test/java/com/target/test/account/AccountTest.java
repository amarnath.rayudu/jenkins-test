package com.target.test.account;

import static org.testng.AssertJUnit.assertTrue;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.target.pages.AccountPage;
import com.target.pages.Homepage;
import com.target.pages.LoginPage;
import com.target.test.base.BaseTest;

public class AccountTest extends BaseTest {

	Homepage homepage;
	LoginPage loginPage;
	AccountPage accountPage;

	@BeforeMethod(alwaysRun = true)
	public void start() throws Exception {
		homepage = new Homepage(driver);
		loginPage = homepage.navigateToLogin();
		assertTrue("Login Page is not loaded", loginPage.isloginPageLoaded());
		homepage = loginPage.login("ramaa.test26@gmail.com", "scmdata1");
	}

	@Test
	public void sampleTest() throws InterruptedException {
		accountPage = homepage.navigateToAccountPage();
		assertTrue("Account Page is not loaded", accountPage.isAccountPageLoaded());
	}

	@AfterMethod(alwaysRun = true)
	public void afterTheTest() throws Exception {
		homepage = accountPage.navigateToHomePage();
		homepage.logout();
	}
}
