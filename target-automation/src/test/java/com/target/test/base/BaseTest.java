package com.target.test.base;

import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;


public class BaseTest {
	public WebDriver driver = new FirefoxDriver();
	private static final Logger logger = LogManager.getLogger(BaseTest.class.getName());

	@BeforeSuite
	public void init() {
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		driver.get("http://www.target.com/");
	}

	
	
	@AfterSuite
	public void teardown() {
		
		driver.quit();
	}

}
